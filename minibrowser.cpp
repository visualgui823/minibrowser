#include "minibrowser.h"
#include <QWebEngineProfile>
#include <QWebEngineSettings>
#include <QJsonObject>
#include <QJsonDocument>
#include <QWebEngineCookieStore>

MiniBrowser::MiniBrowser(QObject *parent) : QWebEngineUrlRequestInterceptor(parent)
{
    view = new QWebEngineView();
    connect(view, SIGNAL(loadFinished(bool)), SLOT(loadFinished(bool)));
    connect(view->page()->profile()->cookieStore(), SIGNAL(cookieAdded(QNetworkCookie)), SLOT(cookieAdded(QNetworkCookie)));
    view->page()->profile()->setHttpCacheType(QWebEngineProfile::MemoryHttpCache);
    view->page()->profile()->setHttpUserAgent("Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36");
    view->page()->profile()->setRequestInterceptor(this);
    view->page()->settings()->setAttribute(QWebEngineSettings::AutoLoadImages, false);
    view->page()->settings()->setAttribute(QWebEngineSettings::AutoLoadIconsForPage, false);
    view->setUrl(QUrl("https://appleid.apple.com/account"));
    // view->show();
}

void MiniBrowser::interceptRequest(QWebEngineUrlRequestInfo &info)
{
    QString file = info.requestUrl().fileName();
    if(file.endsWith(".woff") || file.endsWith(".ttf") || file.endsWith(".css") || file.endsWith("captcha")){
        info.block(true);
    }else{
        info.setHttpHeader("Accept-Language", "zh-CN,zh;q=0.8");
    }
}

void MiniBrowser::loadFinished(bool flag) {
    view->page()->toHtml([=](QString html){
        QJsonObject o;

        o.insert("html", QJsonValue(html));
        o.insert("cookies", cookies);

        QJsonDocument doc;
        doc.setObject(o);
        puts(doc.toJson(QJsonDocument::Compact).data());
        exit(0);
    });
}

void MiniBrowser::cookieAdded(const QNetworkCookie &cookie)
{
    QJsonObject o;
    o.insert("name", QJsonValue(cookie.name().data()));
    o.insert("value", QJsonValue(cookie.value().data()));
    o.insert("domain", QJsonValue(cookie.domain()));
    o.insert("path", QJsonValue(cookie.path()));
    cookies.append(o);
}
