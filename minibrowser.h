#ifndef MINIBROWSER_H
#define MINIBROWSER_H

#include <QObject>
#include <QWebEngineView>
#include <QWebEngineDownloadItem>
#include <QWebEngineUrlRequestInterceptor>
#include <QWebEngineUrlRequestInfo>
#include <QNetworkCookie>
#include <QJsonArray>

class MiniBrowser: public QWebEngineUrlRequestInterceptor
{
    Q_OBJECT
public:
    explicit MiniBrowser(QObject *parent = 0);
    void interceptRequest(QWebEngineUrlRequestInfo &info);

private:
	QWebEngineView *view;
    QWebChannel *channel;
    QJsonArray cookies;

private slots:
    void loadFinished(bool flag);
    void cookieAdded(const QNetworkCookie &cookie);

};

#endif // MINIBROWSER_H
